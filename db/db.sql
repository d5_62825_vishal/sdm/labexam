-- Q1.Containerize MySQL and create book(book_id, book_title, 
-- publisher_name, author_name) table inside that. 
-- book table will be created through db.sql file.
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'manager';
flush privileges;

DROP DATABASE IF EXISTS bookdb ;
CREATE DATABASE bookdb;
USE bookdb;
create table book(book_id int primary key auto_increment,book_title varchar(40),publisher_name VARCHAR(40), author_name VARCHAR(40)); 
INSERT INTO book (book_title,publisher_name, author_name)VALUES('Jungle book','Knowledge Treasure','Rudyard Kipling');
INSERT INTO book (book_title,publisher_name, author_name)VALUES('Digital Electronics','SCHAND','R P Jain');
INSERT INTO book (book_title,publisher_name, author_name)VALUES('Electric circuits','Pearson','Alexander');

