const express =require('express')
const cors = require ('cors')

const app = express()
app.use (cors('*'))
app.use(express.json())

//adding router
const bookrouter=require('./Routes/book')
app.use('/book',bookrouter)
app.listen(4000,'0.0.0.0',() =>{
console.log('server listening on port 4000')
})






