const express = require('express')
const db  = require('../db')
const utils  = require('../utils')

const router  = express.Router();



// -- 1. GET  --> Display Book using name from Containerized MySQL

// -- 2. POST --> ADD Book data into Containerized MySQL table

// -- 3. UPDATE --> Update Publisher name and Author name into Containerized MySQL table

// -- 4. DELETE --> Delete Book from Containerized MySQL

router.get('/',(request,response)=>{
    const {book_title} = request.query;
    
    const query = `select * from book WHERE book_title='${book_title}'`;
    
   
    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.post('/addbook',(request,response)=>{

    const {book_title,publisher_name,author_name} = request.body;

    const query = `insert into book(book_title,publisher_name,author_name) values('${book_title}','${publisher_name}',
    '${author_name}');`

    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.patch('/update/:id',(request,response)=>{

    const {id} = request.params;
  
    const {publisher_name,author_name} = request.body;

    const query = `update book set publisher_name='${publisher_name}',
    author_name='${author_name}' where book_id=${id};`
    //console.log("db=",db,typeof db,query)
    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});


router.delete('/delete/:id',(request,response)=>{

    const {id} = request.params;
  
    const query = `delete from book where book_id = ${id};`

    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

module.exports = router;