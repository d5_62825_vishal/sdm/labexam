const mysql = require('mysql2')

const pool = mysql.createPool({
  //host added in docker-compose.yaml
  host: 'mydb',

  user: 'root',
  password: 'manager',
  database: 'bookdb',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})

module.exports = pool